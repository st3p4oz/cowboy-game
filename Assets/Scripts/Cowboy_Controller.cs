using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cowboy_Controller : MonoBehaviour
{
    private Rigidbody body;
    public float moveSpeed = 1;
    public float jumpSpeed = 1;
    public Vector3 mouse;
    public List<GameObject> cows = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
     body = GetComponent<Rigidbody>();   
    }

    // Update is called once per frame
    void Update()
    {
       float xInput = Input.GetAxis("Horizontal");
        float yInput = Input.GetAxis("Vertical");

        transform.Rotate(0,(xInput),0 * jumpSpeed * Time.deltaTime, Space.Self);



        transform.Translate( Vector3.forward * yInput * moveSpeed * Time.deltaTime,
Space.Self );

    
}
}