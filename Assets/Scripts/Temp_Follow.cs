using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp_Follow : MonoBehaviour
{
    public Transform cowboy;
    public float speed, offset;
    public bool isCorraled = false;
    public bool isInPen = false;

    public List<GameObject> cow_list= new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (isCorraled){
        var step =  speed * Time.deltaTime;
        transform.parent.gameObject.transform.position = Vector3.MoveTowards(transform.position, new Vector3(cowboy.position.x, cowboy.position.y, cowboy.position.z + offset) , step);
        }
        if (isInPen){
            isCorraled = false;
        }
    }
    public void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player"){
            isCorraled = true;
            AddCow(other.gameObject);
        }
    }
    public void AddCow(GameObject player){
        cow_list = player.GetComponent<Cowboy_Controller>().cows;
        cow_list.Add(this.gameObject);
        Debug.Log(cow_list[0]);
        
    }
    
}
