using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pen_Script : MonoBehaviour
{
    public List<GameObject> cow_list = new List<GameObject>();
    public Transform penCenter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EmptyCows(GameObject player){
        cow_list = player.GetComponent<Cowboy_Controller>().cows;
        foreach(GameObject cow in cow_list){
            cow.transform.parent.gameObject.transform.position = penCenter.transform.position;
            cow.GetComponent<Temp_Follow>().isInPen = true;
            Debug.Log(cow);

        }
    }
    public void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player"){
            EmptyCows(other.gameObject);
            Debug.Log("emptying them boyz");
        }
    }
}
