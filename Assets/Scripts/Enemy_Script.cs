using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Script : MonoBehaviour
{
    public float speed;
    public bool isStalking = false;
    public Transform cowboy;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
        
    
    // Update is called once per frame
    void Update()
    {
        if (isStalking){
            var step =  speed * Time.deltaTime;
            //Debug.Log("touch");
            transform.parent.gameObject.transform.position = Vector3.MoveTowards(transform.position, cowboy.transform.position , step);
        }
        
    }
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player"){
            isStalking = true;
            
        }
        }
    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Player"){
            isStalking = false;
        }
        }
    

}
